#!/usr/bin/bash

if rpm -q --quiet kernelshark; then
    :
else
    sudo dnf install -y kernelshark
    if [[ $? != 0 ]]; then
       echo "install of kernelshark failed!"
       exit 1
    fi
fi

echo "kernelshark is GUI tool, just check the installation files"
if [[ ! -f /usr/bin/kernelshark ]]; then
    echo "/usr/bin/kernelshark not found!"
    exit 2
fi

exit 0
